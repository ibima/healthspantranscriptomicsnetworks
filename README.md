This project applies WGCNA to analyse all entries in ArrayExpress that
are described by the term "healthspan" or (for the worm) "health".

Folders:
 * Data: Contains expression data as it is to be analysed
 * Data\_downloaded: Raw data as it was downloaded, like expression data still in a tarball
 * Data\_parameters: Instructions how to process a particular experiment, which needs to be
                     adjusted for every expression data set
 * Data\_external: Auxillary data file, e.g. to match a probe to a gene
 * Logs: Log files
 * Queries
 * Results: Output from the WGCNA workflow and downstream analyses
 * Scripts: R scripts performing the analysis.

The Scripts were/are invoked manually for each dataset. The numeric prefix indicates the order
of the scripts' execution:

 * Scripts/01\_fetchData.sh: 
 * Scripts/02\_unpackData.sh: 
 * Scripts/03\_fetchChipAnnotation.sh: auxillary file is downloaded
 * Scripts/04\_getBioMart\_orthologues.R: 
 * Scripts/20\_WGCNA\_template\_01\_dataInput.R
 * Scripts/30\_WGCNA\_template\_02-networkConstr-auto.R
 * Scripts/40\_WGCNA\_template\_03-relateModsToExt.R
 * Scripts/60\_WGCNA\_template\_05-Visualization.R
 * Scripts/75\_WGCNA\_exportNetworkAndQuantiles.R
 * Scripts/80\_exportGenes.R
 * Scripts/105\_integrate.R
 * Scripts/110\_integrateGenes.R
 * Scripts/200\_network\_statistics.R

Steps to add a dataset to the analysis:

 * Retrieve raw data to Data\_downloaded folder, may be done manually or with the Scripts/01\_fetchData.sh script
 * Place directly interpretable data in a dedicated subdirectory of the Data folder, nested folders are allowed
 * Have envrironment variable WGCNA\_rawDataFolder point to that directory
   ```
   export WGCNA_rawDataFolder=Data/israel/EGEOD54853-CEL
   ```
   If the directory has a dot in its name, only the characters up to that dot will constitute the project identifier.
 * Start R session
   ```
   R --vanilla
   ```
 * Create a parameter file for this project, hereto
   * create a .R file within the Data\_parameters hierarchy that reflects the $WGCNA\_rawDataFolder
   * specify all parameters such that the file can be sourced from within the R code to override the defaults
   ```
cat <<EOCAT > Data_paramters/subdir/$WGCNA_rawDataFolder.R
param.readtable.commentchar <- ""
param.readtable.removeAlphaColums <- FALSE
param.data.type <- ""
param.data.file.suffix<-NA
param.transform.to.logarithmic <- FALSE
EOCAT 
   ```
 * Copy'n'paste the code from Scripts/20\_WGCNA\_template\_01\_dataInput.R
   * follow the code line-by-line
   * override default parameters to adjust to your data and save the same in your parameter file
 * The sample descriptions should be in sdrf as downloaded from ArrayExpress in Data\_downloaded/subdir/$(basename $WGCNA\_rawDataFolder .1).sdrf.txt
   The code will produce a wget command for you if this file is not existing.
 * Continue analogously with Scripts/30\_WGCNA\_template\_02-networkConstr-auto.R
   * Determine power threshold and set that paramter
 * Continue analogously with Scripts/40\_WGCNA\_template\_03-relateModsToExt.R
 * Continue analogously with Scripts/60\_WGCNA\_template\_05-Visualization.R
 * Continue analogously with Scripts/70\_WGCNA\_template\_06-ExportNetwork.R

Once all data has been seen by WGCNA, prepare a summary with
 * Scripts/105\_integrate.R (Table 1 of accompanying paper) and
 * Scripts/110\_integrateGenes.R (Table 2)
 * Scripts/200\_network\_statistics.R
