#!/usr/bin/R

# This code is the adaptation of the WGCNA tutorial for
# a slightly increased flexibility while dealing with
# ArrayExpress files..
#
# This script expected to pass a path to a directory
# containing all files of an ArrayExpress data set, passed
# by the environment variable 'WGCNA_rawDataFolder'. From
# the UNIX shell:
# WGCNA_rawDataFolder="Data/healthspan/E-GEOD-19102.processed.1" R --vanilla
#

source("Scripts/10000_read_params.R")

#=====================================================================================
#
#  Code chunk 1
#
#=====================================================================================


# Display the current working directory
cat("cwd: "); print(getwd())
# Load the WGCNA package
require(WGCNA)
# The following setting is important, do not omit.
options(stringsAsFactors = FALSE);
# Allow multi-threading within WGCNA. This helps speed up certain calculations.
# At present this call is necessary for the code to work.
# Any error here may be ignored but you may want to update WGCNA if you see one.
# Caution: skip this line if you run RStudio or other third-party R environments. 
# See note above.
enableWGCNAThreads()

files.to.open <- list(
   "data" = paste(logsFolder,"/01-dataInput.RData",sep="")
)

cat("I: Loading work from earlier workflow steps in ",logsFolder,".\n",sep="")
for (n in names(files.to.open)) {
  f.full <- files.to.open[[n]]
  if (!file.exists(f.full)) {
    stop(paste("E: Could not find ",n," file at '",f.full,"'.\n",sep=""))
  }
  lnames = load(file = f.full)
  if (0 == length(lnames)) stop(paste("E: Failed to load ",n," file from '",f.full,"'.",sep=""))
  cat("Loaded from ",f.full,": ", paste(lnames,collapse=", "),"\n", sep="")
}

#=====================================================================================
#
#  Code chunk 2 - graphically determine power threshold
#
#=====================================================================================


whoami <- readLines(pipe("whoami"))
hostname <- readLines(pipe("hostname --long"))

for(networkType in c("unsigned","signed","signed hybrid")) {
    thresholdsFile <- paste(logsFolder,"/02-threshold-",gsub(x=networkType," ",""),".pdf",sep="")
    if (file.exists(thresholdsFile) && !is.na(param.power) &&!is.na(param.power.signed)) {
        cat("I: Power already set (= ",param.power,"), thresholdsFile already existing (",thresholdsFile,"). Not redoing the image.\n",sep="")
    } else {
        cat("I: Determining soft-thresholding powers - ",networkType,"\n",sep="")
        # Choose a set of soft-thresholding powers
        powers = c(c(1:10), seq(from = 12, to=30, by=2))
        # Call the network topology analysis function
        sft = pickSoftThreshold(data=datExpr, powerVector = powers, verbose = 5, networkType=networkType)
        # Plot the results:
        if (TRUE) {
            pdf(file=thresholdsFile,width=9,height=5)
        } else {
            sizeGrWindow(9, 5)
        }
        par(mfrow = c(1,2))
        cex1 = 0.9
        # Scale-free topology fit index as a function of the soft-thresholding power
        plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
             xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2",type="n",
             main = paste("Scale independence - ",networkType,sep=""),
             sub = paste(projectCoreName," with ",nrow(datExpr)," samples",sep=""))
        text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
             labels=powers,cex=cex1,col="red")
        # this line corresponds to using an R^2 cut-off of h
        abline(h=0.90,col="red")
        # Mean connectivity as a function of the soft-thresholding power
        plot(sft$fitIndices[,1], sft$fitIndices[,5],
             xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n",
             main = paste("Mean connectivity -",networkType,sep=""),
             sub = projectCoreName," with ",nrow(datExpr)," samples",sep="")
        text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")
        dev.off()
        cat("I: Please inspect generated image at '",whoami,"@",hostname,":",paste(getwd(),thresholdsFile,sep="/"),"'\n",sep="")
    }
}

#=====================================================================================
#
#  Code chunk 3 - create TOM
#
#=====================================================================================

cat("param.power:         ",param.power,"\n")
cat("param.maxBlockSize:  ",param.maxBlockSize,"\n")
cat("param.minModuleSize: ",param.minModuleSize,"\n")
param.TOMFileBase <- paste(logsFolder,"/",paste(projectCoreName,"TOM",sep=""),sep="")
net <- blockwiseModules(datExpr, power = param.power,
                        maxBlockSize = param.maxBlockSize,
                        TOMType = "unsigned", minModuleSize = param.minModuleSize,
                        reassignThreshold = 0, mergeCutHeight = 0.25,
                        numericLabels = TRUE, pamRespectsDendro = FALSE,
                        saveTOMs = TRUE,
                        saveTOMFileBase = param.TOMFileBase, 
                        verbose = 3)
net.signed <- NA
if (!is.naparam.power.signed) {
    net.signed <- blockwiseModules(
                        datExpr, power = param.power.signed,
                        maxBlockSize = param.maxBlockSize,
                        TOMType = "signed", minModuleSize = param.minModuleSize,
                        reassignThreshold = 0, mergeCutHeight = 0.25,
                        numericLabels = TRUE, pamRespectsDendro = FALSE,
                        saveTOMs = TRUE,
                        saveTOMFileBase = paste(param.TOMFileBase,"Signed",sep=""), 
                        verbose = 3)
}


#=====================================================================================
#
#  Code chunk 4
#
#=====================================================================================


# open a graphics window
plotDendroAndColors <- TRUE
if (plotDendroAndColors) {
    dendroAndColorsFile <- paste(logsFolder,"/02-DendroAndColours.pdf",sep="")
    pdf(dendroAndColorsFile,width=12,height=9)
} else {
    sizeGrWindow(12, 9)
}
# Convert labels to colors for plotting
mergedColors = labels2colors(net$colors)
# Plot the dendrogram and the module colors underneath
plotDendroAndColors(net$dendrograms[[1]], mergedColors[net$blockGenes[[1]]],
                    "Module colors",
                    dendroLabels = FALSE, hang = 0.03,
                    addGuide = TRUE, guideHang = 0.05)
dev.off()

if (plotDendroAndColors) {
    cat("I: Please inspect generated image at '",whoami,"@",hostname,":",paste(getwd(),dendroAndColorsFile,sep="/"),"'\n",sep="")
}

#=====================================================================================
#
#  Code chunk 5
#
#=====================================================================================


moduleLabels <- net$colors
moduleColors <- labels2colors(net$colors)
cat("Module color distribution:\n")
print(rle(sort(moduleColors)))

MEs <- net$MEs
geneTree <- net$dendrograms[[1]]
TOMFiles <- net$TOMFiles
save(MEs, moduleLabels, moduleColors, geneTree, saveTOMFileBase=param.TOMFileBase, TOMFiles,
     param.minModuleSize,
     net=net, # as a backup
     net.signed=net.signed,
     file = paste(logsFolder,"/02-networkConstruction-auto.RData",sep=""))

cat("\n\n[ OK ]\n")
