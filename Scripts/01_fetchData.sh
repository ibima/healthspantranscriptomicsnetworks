#!/bin/sh

set -e

files=$*

root=$(dirname $(dirname $(realpath $0)))
destdir="$root"/Data_downloaded/worm

wgetMin="--progress=bar --no-clobber --no-host-directories --no-directories"

if [ ! -d "$destdir" ]; then
	echo "E: Could not find destination folder '$destdir'"
	exit 1
fi

for f in $files
do
	echo "I: Fetching data from file '$f' into '$destdir'"
	if echo $(basename $f) | grep -qi "^arrayExpress"; then
		echo "I: - treatment as an ArrayExpress query result"
		cat $f | while IFS= read -r line
		do
			url=$(echo "$line"|cut -f7)
			name=$(echo "$line"|cut -f1)
			experiment=$(echo "$line"|cut -f3)
			species=$(echo "$line"|cut -f4)
			echo "I: name $name, experiment $experiment, species $species"
			if echo "$name" | grep -q Accession
			then
				continue
			fi
			if echo "$experiment" | grep -q "ChIP-seq"
			then
				echo "I: Not downloading ChIP-seq data"
				continue
			fi

			if echo "$url" | grep -q available; then
				echo "W: No data for '$name'"
			else
				continue
				echo "I: Downloading data for '$name' from '$url'"
				if echo "$url" | grep "zip$"; then
					echo "zip"
					wget --directory-prefix=$destdir $wgetMin "$url"
				else
					echo "folder of zips"
					wget --directory-prefix=$destdir/$name $wgetMin --recursive --level=1 --accept=zip "$url"
				fi
			fi
		done
	fi
done
