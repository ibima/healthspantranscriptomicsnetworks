#!/bin/bash

cat <<EOHELP
   This script expects a number of .zip files to be extracted that are passed as an argument.
EOHELP

set -e

destdir=$(dirname $0)/../Data/worm
if [ ! -d "$destdir" ]; then
	echo "I: Creating directory '$destdir' where unpacked files will reside"
	mkdir "$destdir"
fi

echo A
for i in $*
do
	newdirname=$(basename $i .zip)
	sourcedir=$(dirname $i)
	finaldestdir="$destdir"/"$newdirname"
	if [ -d "$finaldestdir" ]; then
		echo "W: New folder '$finaldestdir' is already existing - skipping unpacking of $i"
	fi
	mkdir "$finaldestdir"
	ireal=$(realpath $i)
	unzip $i -d "$finaldestdir"
done
