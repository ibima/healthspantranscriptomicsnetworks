#!/bin/sh

set -e

root=$(dirname $(dirname $(realpath $0)))
destdir="$root"/Data_external

wgetMin="--no-clobber --no-host-directories --no-directories"

#MouseRef-8 v2 Expression BeadChip
wget $wgetMin --directory-prefix=$destdir https://emea.support.illumina.com/content/dam/illumina-support/documents/downloads/productfiles/mouseref-8/mouseref-8_v2_0_r3_11278551_a.zip
