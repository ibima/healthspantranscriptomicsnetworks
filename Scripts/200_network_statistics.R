

library(STRINGdb)
string_db <- STRINGdb$new( version="11", species=9606,score_threshold=900, input_directory="")
string_db
g <- string_db$get_graph()
g.degree <- degree(g,V(g),"all")
quantile(g.degree,c(0,0.25,0.5,0.75,0.9,0.95,0.99,1))
# 200
#      0%      25%      50%      75%      90%      99%     100% 
#    2.00   276.00   536.00   940.00  1508.00  2959.14 10926.00 
# 900
#  0%  25%  50%  75%  90%  95%  99% 100% 
#   2    8   30  130  316  452  688 2704 


g.degree.sorted <- sort(g.degree)
length(g.degree.sorted)
# 200
# 19344
# 900
# 12396

genes.hub <- c("ACTN3","ANK1","MRPL18","MYL1","PAXIP1","PPP1CA","SCN3B","SDCBP","SKIV2L","TUBG1","TYROBP","WIPF1")
genes.hub.string <- string_db$mp( genes.hub )

ranks.hub <- which(names(g.degree.sorted) %in% genes.hub.string)
# 9713 13900 14055 15404 15866 16069 16913 17459 17475 17708 17894 19237

# P should be at 0.5^12 to all be in the upper half
V.names <- names(V(g))
perm.hub <- sapply(1:10000,function(X){
   if (0 == X %% 10) cat(".")
   random.genes <- sample(V.names,size=length(genes.hub.string))
   pos <- which(names(g.degree.sorted) %in% random.genes)
   return(sum(pos))
})

quantile(perm.hub)
#200
#      0%      25%      50%      75%     100% 
# 50913.0 102864.8 116139.0 128948.2 182540.0 
#900
#      0%      25%      50%      75%     100% 
# 26005.0  65891.0  74377.0  82826.5 119370.0 

sum(ranks.hub)
#200
# 191693
#900
# 109410
sum(perm.hub>sum(ranks.hub))/10000
# 0.0022



genes.phen <- c("AC068831.7", "ADAM10", "APBB1IP", "CEBPB", "CREBBP", "EIF3F", "INTS12", "KPNA3", "MEX3C", "MRPL19", "MYL1", "PAXIP1", "PCNX2", "PPP1CA", "PPP1CB", "PPP2R3C", "RAB2A", "RAB31", "RPL29", "RTN2", "RYR1", "SCN3B", "SIX4", "SNRPD1", "TMEM70", "TUBG1", "WIPF1", "ZC3H15", "SQSTM1")
genes.phen.string <- string_db$mp( genes.phen)

ranks.phen <- which(names(g.degree.sorted) %in% genes.phen.string)
perm.phen <- sapply(1:10000,function(X){
   if (0 == X %% 10) cat(".")
   random.genes <- sample(V.names,size=length(genes.phen.string))
   pos <- which(names(g.degree.sorted) %in% random.genes)
   return(sum(pos))
})

quantile(perm.phen)
sum(ranks.phen)
sum(perm.phen>sum(ranks.phen))/10000
