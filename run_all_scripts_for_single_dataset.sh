#!/bin/bash

# This script executes all in Scripts/* that is relevant for
# data found in the Data directory. The environment variable
# WGCNA_rawDataFolder can be set if there are no arguments.
# Otherwise, all scripts are executed with WGCNA_rawDataFolder
# set to each of the arguments.
# 
# Try executing this as
#    WGCNA_rawDataFolder="Data/healthspan/E-GEOD-19102.processed.1"
#

set -e

if [ "-h" = "$1" -o "--help" = "$1" ]; then
    cat <<EOHELP

$(basename $0) - runs all in Script for single dataset

Usage: $(basename $0) --help | -h
       $(basename $0) < list of data directories >

Each of the argument is interpreted as a Data directory and subject to
all scripts in the Scripts directory.

EOHELP
    exit 1
fi

for datadir in $*
do
    for script in 20_WGCNA_template_01_dataInput.R 30_WGCNA_template_02-networkConstr-auto.R 40_WGCNA_template_03-relateModsToExt.R 60_WGCNA_template_05-Visualization.R 75_WGCNA_exportNetworkAndQuantiles.R 80_exportGenes.R
    do
        echo
        echo "***"
        echo "***     [ START ] $script"
        echo "***"
        echo

    	if ! WGCNA_rawDataFolder="$datadir" R --vanilla < Scripts/$script; then
    	   echo "E: execution failed. Rerun last executed script as"
           echo "   'WGCNA_rawDataFolder="$datadir" R --vanilla < Scripts/$script'"
	   exit $?
	fi

        echo "***"
        echo "***     [ DONE ] $script"
        echo "***"
    done
done
