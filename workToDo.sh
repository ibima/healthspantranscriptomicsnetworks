#!/bin/bash -e

cd $(dirname $0)

parameterFiles=$(find Data_parameters -name "*.R"|grep -v "global.R" )
for parameterFile in $parameterFiles
do
   dataDir=$( echo $parameterFile | sed -e "s/.R$//" -e s/^Data_parameters/Data/| sed -e "s/.R$//" -e s/^Data_parameters/Data/ )
   echo -n "I dataDir: $dataDir"
   resultsDir="Results/$(basename  -s .R $parameterFile)"
   echo -n " Results:"
   if [ -d "$resultsDir" ]; then
      if ls "$resultsDir" | grep -qi VisANT; then
          echo -n " VisANT"
      fi
      if ls "$resultsDir" | grep -qi genes; then
          echo -n " Genes"
      fi
      echo
   else
      echo " none (expected '$resultsDir')"
   fi
done
