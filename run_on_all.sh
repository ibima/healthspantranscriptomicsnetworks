#!/bin/bash

Workdir=$(realpath $(dirname $0))
echo "I: Workdir: $Workdir"
executeDirectly=

if [ -z "$Rscript" ]; then
#   Rscript="Scripts/75_WGCNA_exportNetworkAndQuantiles.R"
#   Rscript="Scripts/20_WGCNA_template_01_dataInput.R"

#   Rscript="Scripts/30_WGCNA_template_02-networkConstr-auto.R"
#   RscriptInput="01-dataInput.RData"
#   RscriptOutput="02-networkConstruction-auto.RData"

#   Rscript="Scripts/40_WGCNA_template_03-relateModsToExt.R"
#   RscriptInput="02-networkConstruction-auto.RData"
#   RscriptOutput="03-relateModsToExt.RData"

#   Rscript="Scripts/75_WGCNA_exportNetworkAndQuantiles.R"
#   RscriptInput="03-relateModsToExt.RData"
#   RscriptOutput="75-TOMforModules.RData"

#   Rscript="Scripts/80_exportGenes.R"
#   RscriptInput="75-TOMforModules.RData"
   #RscriptOutput="75-mouse-VisANT-Threshold-green.txt"

   Rscript="all"
   RscriptOutput="80.txt"

   echo "I: Setting Rscript to '$Rscript'"
fi

#RscriptID=$(basename "$Rscript" | cut -f1 -d'_')
RscriptID="all"

if [ -z "$Time" ]; then
   #Time="0-0:30"  # 80_WGCNA
   #Time="0-2"     # 20_WGCNA
   #Time="0-4"     # 30_WGCNA
   #Time="2-12"     # 75_WGCNA
   Time="0-9:30"  # all
   #Time="14-00:00:0"
   echo "I: Setting slurm Time to '$Time'"
fi

if [ -z "$Mem" ]; then
   Mem=55000
   echo "I: Setting slurm Mem(ory) to '$Mem'"
fi

if [ -z "$Account" ]; then
   Account=cluster
   echo "I: Setting slurm Account to '$Account'"
fi
if [ -z "$Partition" ]; then
   #Partition=interactive
   Partition=requeue
   echo "I: Setting slurm Partition to '$Partition'"
else
   echo "I: Using suggested partition '$Partition'"
fi

if [ -n "$1" ]; then
      if [ "-h" = "$1" -o "--help" = "$1" -o "-help" = "$1" -o "help" = "$1" ]; then
      cat <<EOHELP
Usage: $(basename $0) [jobnames for predictions to run]
       $(basename $0) --list
       $(basename $0) --help

This script supports the submission of jobs to _perform_ a particular
_WGCNA partial run_ on a SLURM cluster.  Hereto it wraps the R script
"$Rscript".
The communication of the command line with that R script is performed via
envrionment variables. Especially the there is the environment variable
WGCNA_rawDataFolder that defines on what data set this shall be run.

Parameters:
  Rscript=$Rscript
  Workdir=$Workdir
  Time=$Time
  Mem=$Mem
  Partition=$Partition
  Account=$Account
  Nodes=$Nodes
EOHELP
        exit
    fi

    if [ "-l" = "$1" -o "--list" = "$1" -o "-list" = "$1" -o "list" = "$1" ]; then
        echo "The following jobs are possible:"
        #for d in $(find Logs/ -mindepth 1 -maxdepth 1 -type d)
        for d in $(find -L Data/ -mindepth 2 -maxdepth 2 -type d)
        do
            if echo "$d" | grep -q "/skip"; then
                echo "I: Skipping '$d'"
                continue
            fi
            echo -n "Directory '$d':"
            if [ -n "$RscriptOutput" -a -r "Logs/$(basename $d)/$RscriptOutput" ]; then
                echo " [done] - found 'Logs/$(basename $d)/$RscriptOutput'"
            elif [ -n "$RscriptsOutput" -a -r "Results/$(basename $d)/$RscriptOutput" ]; then
                echo " [done] - found 'Results/$(basename $d)/$RscriptOutput'"
            elif [ -n "$RscriptsInput" -a ! -r "Logs/$(basename $d)/$RscriptsInput" ]; then
                echo " [no input] - looked for 'Logs/$(basename $d)/$RscriptInput'"
                echo "              Check execution of prior elements of workflow"
            else
                echo " [missing]"
                if [ -n "$executeDirectly" ]; then
                    echo "     Run 'WGCNA_rawDataFolder=\"$d\" R --vanilla < $Rscript"
                else
                    echo "     $0 $d"
                fi
            fi
        done
        echo "[end]"
        exit
    fi
fi

if [ -n "$WGCNA_rawDataFolder" ]; then
    if [ "all" = "$WGCNA_rawDataFolder" ]; then
        run_all_scripts_for_single_dataset.sh "$WGCNA_rawDataFolder"
    else
    # Meant to be executed from the queueing system - but does not need to be
        echo "source('$Rscript');" | R --vanilla
    fi
else
    for d in $*
    do
        echo -n "Directory '$d':"
        if [ "all" = "$RscriptID" ]; then
            echo Executing: sbatch --account=$Account --partition $Partition --time=$Time --chdir=$Workdir --cpus-per-task=1 --job-name=all_$d -n 1 --cores-per-socket=1 --threads-per-core=1 --output=slurm_output_wgcna_${RscriptID}_$(basename $d).txt --mem=$Mem $Nodes run_all_scripts_for_single_dataset.sh "$d"
            sbatch --account=$Account --partition $Partition --time=$Time --chdir=$Workdir --cpus-per-task=1 --job-name=all_$d -n 1 --cores-per-socket=1 --threads-per-core=1 --output=slurm_output_wgcna_${RscriptID}_$(basename $d).txt --mem=$Mem $Nodes run_all_scripts_for_single_dataset.sh "$d"
        else
            echo Executing: WGCNA_rawDataFolder="$d" sbatch --account=$Account --partition $Partition --time=$Time --chdir=$Workdir --cpus-per-task=1 --job-name=WGCNA_75_$d -n 1 --cores-per-socket=1 --threads-per-core=1 --output=slurm_output_wgcna_${RscriptID}_$(basename $d).txt --mem=$Mem $Nodes ./$(basename $0)
            WGCNA_rawDataFolder="$d" sbatch --account=$Account --partition $Partition --time=$Time --chdir=$Workdir --cpus-per-task=1 --job-name=WGCNA_75_$d -n 1 --cores-per-socket=1 --threads-per-core=1 --output=slurm_output_wgcna_${RscriptID}_$(basename $d).txt --mem=$Mem $Nodes ./$(basename $0)
        fi
        date
    done
fi
